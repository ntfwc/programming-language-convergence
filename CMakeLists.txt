cmake_minimum_required(VERSION 2.8)
include(FindPkgConfig)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake")

project(Launcher)
set(LAUNCHER_EXE launcher)
add_executable(${LAUNCHER_EXE} src/launcher.c)
target_link_libraries(${LAUNCHER_EXE} ${CMAKE_DL_LIBS})

set(RUN_PY_TARGET_LIB run_py)
project(CPP_lib)
find_package(Lua REQUIRED)
find_package(MozJs24 REQUIRED)

set(CPP_TARGET_LIB cpp)
add_library(${CPP_TARGET_LIB} MODULE src/lib.cpp src/run-js.cpp)
set_property(TARGET ${CPP_TARGET_LIB} PROPERTY CXX_STANDARD 11)
target_link_libraries(${CPP_TARGET_LIB} ${RUN_PY_TARGET_LIB} ${LUA_LIBRARY} ${MOZJS_LIBRARY} )
target_include_directories(${CPP_TARGET_LIB} PRIVATE ${LUA_INCLUDE_DIR})
target_include_directories(${CPP_TARGET_LIB} SYSTEM PRIVATE ${MOZJS_INCLUDE_DIR})

project(Run_py_lib)

find_program(PYTHON_CONFIG python3m-config)
if(PYTHON_CONFIG)
	message(STATUS "Found python config binary, using it for cflags")
	execute_process(COMMAND ${PYTHON_CONFIG} --cflags OUTPUT_VARIABLE PYTHON_CFLAGS)
	execute_process(COMMAND ${PYTHON_CONFIG} --ldflags OUTPUT_VARIABLE PYTHON_LDFLAGS)
endif()

add_library(${RUN_PY_TARGET_LIB} SHARED src/run-py.c)
if(PYTHON_CFLAGS)
	message(STATUS "PYTHON_CFLAGS=${PYTHON_CFLAGS}")
	string(REPLACE " " ";" PYTHON_CFLAGS ${PYTHON_CFLAGS})
	target_compile_options(${RUN_PY_TARGET_LIB} PRIVATE ${PYTHON_CFLAGS})
	message(STATUS "PYTHON_LDFLAGS=${PYTHON_LDFLAGS}")
	string(STRIP ${PYTHON_LDFLAGS} PYTHON_LDFLAGS)
	string(REPLACE " " ";" PYTHON_LDFLAGS ${PYTHON_LDFLAGS})
	set(PY_TARGET_LIB_DEPS ${PYTHON_LDFLAGS})
else()
	find_package(PythonLibs 3 REQUIRED)
	target_include_directories(${RUN_PY_TARGET_LIB} PRIVATE ${PYTHON_INCLUDE_DIR})
	set(PY_TARGET_LIB_DEPS ${PYTHON_LIBRARY})
endif()
target_link_libraries(${RUN_PY_TARGET_LIB} PRIVATE ${PY_TARGET_LIB_DEPS})

set(RUN_MONO_LIB run_mono)

project(rust_lib)
set(RUST_LIB librust.so)
add_custom_target(rust_lib ALL DEPENDS ${RUST_LIB})
set(RUST_SOURCE_FILE src/rust.rs)
add_custom_command(OUTPUT ${RUST_LIB}
	COMMAND rustc -C prefer-dynamic -C link-args="-Wl,-rpath,${PROJECT_BINARY_DIR}" -L. -l ${RUN_MONO_LIB} --crate-type dylib -o ${RUST_LIB} "${PROJECT_SOURCE_DIR}/${RUST_SOURCE_FILE}"
	MAIN_DEPENDENCY ${RUST_SOURCE_FILE}
	DEPENDS ${RUN_MONO_LIB}
	)

project(run_java_lib)
find_package(OpenJdk REQUIRED)
set(RUN_JAVA_LIB run_java)
add_library(${RUN_JAVA_LIB} MODULE src/run-java.cpp)
target_include_directories(${RUN_JAVA_LIB} PRIVATE ${OPENJDK_INCLUDE_DIR})
target_link_libraries(${RUN_JAVA_LIB} ${OPENJDK_JVM_LIBRARY})
set(CLASS_FILE Main.class)

find_package(Java REQUIRED)
add_custom_target(java_class ALL DEPENDS ${CLASS_FILE})
set(JAVA_SOURCE_FILE src/Main.java)
add_custom_command(OUTPUT ${CLASS_FILE}
	COMMAND ${Java_JAVAC_EXECUTABLE} -d . "${PROJECT_SOURCE_DIR}/${JAVA_SOURCE_FILE}"
	MAIN_DEPENDENCY ${JAVA_SOURCE_FILE})

project(mono_lib)

pkg_search_module(MONO2 REQUIRED mono-2)

add_library(${RUN_MONO_LIB} SHARED src/run-mono.c)
target_link_libraries(${RUN_MONO_LIB} ${MONO2_LDFLAGS})
target_compile_options(${RUN_MONO_LIB} PRIVATE ${MONO2_CFLAGS})

set(C_SHARP_SOURCE_FILE src/lib.cs)
set(C_SHARP_LIB mono.dll)
add_custom_target(c_sharp ALL DEPENDS ${C_SHARP_LIB})
add_custom_command(OUTPUT ${C_SHARP_LIB}
	COMMAND mcs -out:${C_SHARP_LIB} "${PROJECT_SOURCE_DIR}/${C_SHARP_SOURCE_FILE}"
	MAIN_DEPENDENCY ${C_SHARP_SOURCE_FILE})

project(end_lib)
add_library(end MODULE src/end.c)
