#include <Python.h>
#include <stdio.h>

void runFile(const char* filePath)
{
	FILE *f = fopen(filePath, "r");
	if (f == NULL)
	{
		perror(filePath);
		return;
	}
	PyRun_SimpleFile(f, filePath);
	fclose(f);
}

void runPython(void)
{
	Py_Initialize();
	runFile("../src/script.py");
	Py_Finalize();
}
