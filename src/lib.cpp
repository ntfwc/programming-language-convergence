#include <iostream>
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

#include "run-js.hpp"

void print(const char* text)
{
	std::cout << "C++: " << text << std::endl;
}

int runJavascript(lua_State *)
{
	runJsFile("../src/script.js");
	return 0;
}

void runLua()
{
	lua_State *luaState = luaL_newstate();
	luaL_openlibs(luaState);
	lua_register(luaState, "runJavascript", &runJavascript);
	int result = luaL_dofile(luaState, "../src/script.lua");
	if (result != 0)
	{
		fprintf(stderr, "Lua error: %s\n", lua_tostring(luaState, -1));
	}
	lua_close(luaState);
}

extern "C" void run()
{
	print("Hello, Worlds!");
	runLua();
	print("Goodbye");
}

