using System;
using System.Runtime.InteropServices;

public class MainClass
{
	[DllImport("libend.so")]
	protected static extern void end();

	static private void Print(string text)
	{
		Console.WriteLine("C#: {0}", text);
	}

	static public void Run()
	{
		Print("Managed code is here");
		end();
		Print("Goodbye");
	}

	static public void Main()
	{
		Run();
	}
}
