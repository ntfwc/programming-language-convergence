#include <stdio.h>

static void print(char *text)
{
	printf("C: %s\n", text);
}

void end(void)
{
	print("We have reached the end of the call stack, time to say...");
}
