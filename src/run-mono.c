#include <stdio.h>
#include <mono/jit/jit.h>
#include <mono/metadata/mono-config.h>
#include <mono/metadata/assembly.h>

const char *LIB_NAME = "mono.dll";
const char *MAIN_CLASS_NAME = "MainClass";
const char *METHOD_NAME = "Run";


static void run_assembly(MonoDomain *domain)
{
	MonoAssembly *assembly = mono_domain_assembly_open(domain, LIB_NAME);
	if (assembly == NULL)
	{
		static char errorMsg[100];
		snprintf(errorMsg, sizeof(errorMsg), "Failed to open assembly '%s'", LIB_NAME);
		perror(errorMsg);
		return;
	}

	MonoImage *image = mono_assembly_get_image(assembly);
	if (assembly == NULL)
	{
		fprintf(stderr, "Failed to get assembly image\n");
		return;
	}

	MonoClass *class = mono_class_from_name(image, "", MAIN_CLASS_NAME);
	if (class == NULL)
	{
		fprintf(stderr, "Failed to find class '%s'\n", MAIN_CLASS_NAME);
		return;
	}

	MonoMethod *method = mono_class_get_method_from_name(class, METHOD_NAME, 0);
	if (method == NULL)
	{
		fprintf(stderr, "Failed to find method '%s'\n", METHOD_NAME);
		return;
	}

	mono_runtime_invoke(method, NULL, NULL, NULL);
}

void run_mono(void)
{
	MonoDomain *domain;
	mono_config_parse(NULL);
	domain = mono_jit_init(LIB_NAME);
	run_assembly(domain);
	mono_jit_cleanup(domain);
}
