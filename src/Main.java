import java.io.File;

public class Main
{
	static {
		System.loadLibrary("rust");
	}

	public static native void runRust();

	private static void print(String text)
	{
		System.out.println("Java: " + text);
	}

	public static void run()
	{
		print("Just in time");
		runRust();
		print("Goodbye");
	}

	public static void main(String[] args)
	{
		run();
	}
}
