#include <jsapi.h>
#include <stdio.h>
#include <string>
#include <fstream>
#include <memory>
#include "run-py.h"

using namespace JS;

static JSClass global_class = {
	"global",
	JSCLASS_GLOBAL_FLAGS,
	JS_PropertyStub,
	JS_DeletePropertyStub,
	JS_PropertyStub,
	JS_StrictPropertyStub,
	JS_EnumerateStub,
	JS_ResolveStub,
	JS_ConvertStub,
};

static void print(const char *text)
{
	printf("%s\n", text);
}

static std::unique_ptr<std::string> readFile(const std::string& filePath)
{
	std::ifstream ifs(filePath);
	if (ifs.fail())
	{
		perror(filePath.c_str());
		return nullptr;
	}

	std::unique_ptr<std::string> content(new std::string( (std::istreambuf_iterator<char>(ifs) ),
							      (std::istreambuf_iterator<char>() )));
	if (ifs.bad())
	{
		perror(filePath.c_str());
		return nullptr;
	}

	return content;
}

static JSBool myjs_print(JSContext *cx, unsigned argc, Value *vp)
{
	CallArgs args = CallArgsFromVp(argc, vp);
	if (args.length() != 1)
	{
		JS_ReportError(cx, "print needs 1 argument");
		return false;
	}

	Value argVal = args.get(0);
	if (!argVal.isString())
	{
		JS_ReportError(cx, "print must be given a string");
		return false;
	}

	char* argStr = JS_EncodeString(cx, argVal.toString());

	print(argStr);

	JS_free(cx, argStr);

	args.rval().setUndefined();
	return true;
}

static JSBool myjs_runPython(JSContext *cx, unsigned argc, Value *vp)
{
	CallArgs args = CallArgsFromVp(argc, vp);
	if (args.length() != 0)
	{
		JS_ReportError(cx, "runPython takes no arguments");
		return false;
	}

	runPython();

	args.rval().setUndefined();
	return true;
}

static JSFunctionSpec myjs_global_functions[] = {
	JS_FS("print", &myjs_print, 1, 0),
	JS_FS("runPython", &myjs_runPython, 0, 0),
	JS_FS_END
};

static void reportError(JSContext *, const char *message, JSErrorReport *report)
{
	fprintf(stderr, "%s:%u:%s\n",
			report->filename ? report->filename : "[no filename]",
			(unsigned int) report->lineno,
			message);
}

static void evaluateFile(JSContext *cx, const char *file, RootedObject *global)
{
	std::unique_ptr<std::string> content = readFile(file);
	if (content == nullptr)
		return;

	const char* content_c = content->c_str();

	int lineno = 1;
	RootedValue rval(cx);
	JS_EvaluateScript(cx, *global, content_c, strlen(content_c), file, lineno, rval.address());
}

static void runInContext(JSContext *cx, const char *file)
{
	JSAutoRequest ar(cx);

	RootedObject global(cx, JS_NewGlobalObject(cx, &global_class, nullptr));
	if (!global)
	{
		fprintf(stderr, "initialization of global object failed");
		return;
	}


	JSAutoCompartment ac(cx, global);
	JS_InitStandardClasses(cx, global);

	if (!JS_DefineFunctions(cx, global, myjs_global_functions))
		return;

	evaluateFile(cx, file, &global);
}

void runJsFile(const char *file)
{
	JSRuntime *rt = JS_NewRuntime(8L * 1024 * 1024, JS_USE_HELPER_THREADS);
	if (!rt)
	{
		fprintf(stderr, "JSRuntime initialization failed");
		return;
	}

	JSContext *cx = JS_NewContext(rt, 8192);
	if (!cx)
	{
		fprintf(stderr, "JSContext initialization failed");
		return;
	}
	JS_SetErrorReporter(cx, &reportError);

	runInContext(cx, file);

	JS_DestroyContext(cx);
	JS_DestroyRuntime(rt);
	JS_ShutDown();
}

