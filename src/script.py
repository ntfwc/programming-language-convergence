import ctypes

def py_print(text):
    print("Python: " + text)

def runJava():
    lib = ctypes.cdll.LoadLibrary("./librun_java.so")
    lib.runJava()

def main():
    py_print("Slithering in")
    runJava()
    py_print("Goodbye")

if __name__ == "__main__":
    main()
