#include <stdio.h>
#include <dlfcn.h>

void print(const char* text)
{
	printf("C: %s\n", text);
}

void callMethod(void *lib, const char *methodName)
{
	void (*funcptr)(void) = dlsym(lib, methodName);
	if (funcptr == NULL)
	{
		fprintf(stderr, "dlsym: %s\n", dlerror());
		return;
	}
	(*funcptr)();
}

void linkAndRunLib(void)
{
	print("Linking...");

	void *lib = dlopen("./libcpp.so", RTLD_NOW | RTLD_GLOBAL);
	if (lib == NULL)
	{
		fprintf(stderr, "dlopen: %s\n", dlerror());
		return;
	}
	callMethod(lib, "run");
}

int main()
{
	print("Hello, World!");
	linkAndRunLib();
	print("Goodbye");
	return 0;
}
