use std::any::Any;

fn print(text: &str)
{
	println!("Rust: {}", text);	
}

#[no_mangle]
pub extern fn Java_Main_runRust(_env: *const Any, _jclass: *const Any)
{
	run_rust();
}

extern
{
    fn run_mono();
}

#[no_mangle]
pub extern fn run_rust()
{
	print("Running with safely in mind");
        unsafe
        {
            run_mono();
        }
	print("Goodbye");
}
