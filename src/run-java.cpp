#include <stdio.h>
#include <string.h>
#include <jni.h>
#include <signal.h>

const char *classPathOption = "-Djava.class.path=.";
const char *libraryPathOption = "-Djava.library.path=.";

static void setSigAction(int signum, __sighandler_t saHandler, struct sigaction *oldact)
{
	struct sigaction action;
	memset(&action, 0, sizeof(action));

	action.sa_handler = saHandler;
	sigaction(signum, &action, oldact);
}

static void disableSegmentationFault(struct sigaction *oldact)
{
	setSigAction(SIGSEGV, SIG_IGN, oldact);
}

static void reEnableSegmentationFault(const struct sigaction *oldact)
{
	sigaction(SIGSEGV, oldact, NULL);
}

static bool initJvm(JavaVM **jvm, JNIEnv **env)
{
	JavaVMInitArgs vm_args;

	memset(&vm_args, 0, sizeof(vm_args));

	JavaVMOption options[2];
	options[0].optionString = const_cast<char *>(classPathOption);
	options[1].optionString = const_cast<char *>(libraryPathOption);
	vm_args.version = JNI_VERSION_1_8;
	vm_args.nOptions = 2;
	vm_args.options = options;
	vm_args.ignoreUnrecognized = JNI_TRUE;

	struct sigaction oldAction;
	disableSegmentationFault(&oldAction);
	bool result = JNI_CreateJavaVM(jvm, (void**) env, &vm_args) == JNI_OK;
	reEnableSegmentationFault(&oldAction);
	return result;
}

static bool handleException(JNIEnv *env)
{
	if (env->ExceptionCheck())
	{
		env->ExceptionDescribe();
		env->ExceptionClear();
		return true;
	}
	return false;
}

static void runClass(JNIEnv *env)
{
	jclass cls = env->FindClass("Main");
	if (handleException(env))
		return;
	if (cls == NULL)
	{
		fprintf(stderr, "Failed to find class\n");
		return;
	}

	jmethodID mid  = env->GetStaticMethodID(cls, "run", "()V");
	if (handleException(env))
		return;
	if (mid == NULL)
	{
		fprintf(stderr, "Failed to find method\n");
		return;
	}

	env->CallStaticVoidMethod(cls, mid);
	if (handleException(env))
		return;
}

extern "C" void runJava(void)
{
	JavaVM *jvm;
	JNIEnv *env;
	if (!initJvm(&jvm, &env))
	{
		fprintf(stderr, "JVM initialization failed\n");
		return;
	}
	runClass(env);
	jvm->DestroyJavaVM();
}
