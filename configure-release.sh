#!/bin/sh
SCRIPT_DIR=$(dirname "$0")
cd "$SCRIPT_DIR"

set -e
[ -e build-release ] || mkdir build-release
cd build-release

export CC=/usr/bin/clang
export CXX=/usr/bin/clang++
export CFLAGS='-Wall -Wextra -O2 -pipe'
export CXXFLAGS="$CFLAGS"

cmake -G Ninja ..
