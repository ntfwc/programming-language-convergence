#!/bin/sh
SCRIPT_DIR=$(dirname "$0")
cd "$SCRIPT_DIR"

set -e
[ -e build ] || mkdir build
cd build

export CC=/usr/bin/clang
export CXX=/usr/bin/clang++
export CFLAGS='-fsanitize=address -Wall -Wextra -g'
export CXXFLAGS="$CFLAGS"

cmake -G Ninja ..
