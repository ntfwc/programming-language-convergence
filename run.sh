#!/bin/sh
SCRIPT_DIR=$(dirname "$0")
cd "$SCRIPT_DIR"

set -e
handleConfigure()
{
	if [ ! -e build/build.ninja ]
	then
		./configure.sh
		return
	fi
	if [ configure.sh -nt build/build.ninja ]
	then
		rm -r build
		./configure.sh
		return
	fi
}
handleConfigure

./build.sh
export LSAN_OPTIONS=suppressions=$(readlink -f leak.supp)
export ASAN_OPTIONS=allow_user_segv_handler=1
# Unfortunately, the mono version we have on Ubuntu 16.04 has bugs with the GC
# when embedding that prevent mono from initializing.
export GC_DONT_GC=1

cd build
./launcher
