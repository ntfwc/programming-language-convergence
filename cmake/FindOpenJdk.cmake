#.rst:
# FindOpenJdk
# --------
#
# Find OpenJdk
#
# This module finds OpenJdk if it is installed and determines where the
# include files and libraries are.
#
# This code sets the following
# ::
#
#   OPENJDK_FOUND        = if the development files are found
#   OPENJDK_DIR          = The main directory of the jdk
#   OPENJDK_INCLUDE_DIR  = path to where the header files are
#   OPENJDK_JVM_LIBRARY  = path to the jvm library

# Find the openjdk directory

set(OPENJDK_JVM_PATHS /usr/lib/jvm)
foreach(jvmpath ${OPENJDK_JVM_PATHS})
	file(GLOB OPENJDK_DIR ${jvmpath}/java-*-openjdk-*)
	if (OPENJDK_DIR)
		break()
	endif()
endforeach()

if(OPENJDK_DIR)
	message(STATUS "OpenJDK directories=${OPENJDK_DIR}")
	list(GET OPENJDK_DIR 0 OPENJDK_DIR)
	message(STATUS "OpenJDK directory=${OPENJDK_DIR}")

	find_path(OPENJDK_INCLUDE_DIR jni.h
		PATHS ${OPENJDK_DIR}/include)
	if (OPENJDK_INCLUDE_DIR)
		set(OPENJDK_INCLUDE_DIR "${OPENJDK_INCLUDE_DIR};${OPENJDK_INCLUDE_DIR}/linux;${OPENJDK_INCLUDE_DIR}/windows")
	endif()
	find_library(OPENJDK_JVM_LIBRARY jvm 
		PATHS ${OPENJDK_DIR}/jre/lib/server
		      ${OPENJDK_DIR}/jre/lib/amd64/server)
endif()

unset(OPENJDK_JVM_PATHS)

# handle the QUIETLY and REQUIRED arguments and set OPENJDK_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(OpenJdk REQUIRED_VARS OPENJDK_JVM_LIBRARY OPENJDK_INCLUDE_DIR)

mark_as_advanced(OPENJDK_JVM_LIBRARY OPENJDK_INCLUDE_DIR)
