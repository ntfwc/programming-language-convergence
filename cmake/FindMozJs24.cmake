#.rst:
# FindMozJs
# --------
#
# Find MozJs24
#
# This module finds the mozilla javascript engine, spidermonkey, if it is
# installed and determines where the include files and libraries are.
#
# This code sets the following
# ::
#
#   MOZJS24_FOUND        = if the development files are found
#   MOZJS_INCLUDE_DIR  = path to where the header files are
#   MOZJS_LIBRARY      = path to the js library

find_path(MOZJS_INCLUDE_DIR jsapi.h
	PATH_SUFFIXES include/mozjs-24)
find_library(MOZJS_LIBRARY mozjs-24)

# handle the QUIETLY and REQUIRED arguments and set MOZJS24_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(MozJs24 REQUIRED_VARS MOZJS_LIBRARY MOZJS_INCLUDE_DIR)

mark_as_advanced(MOZJS_INCLUDE_DIR MOZJS_LIBRARY)
