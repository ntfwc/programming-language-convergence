## Let us assemble and compute together ##

# Description #

This project runs methods in 8 programming languages, all in one process, in one call stack.

# Purpose #

I made this for a few reasons. One is that I thought it would be cool. More practically, after learning how to embed Lua, I wanted to learn how to embed more languages. So this can serve as a sort of reference. For the higher level languages, it also shows how to call native code from them. Another reason is that I wanted to learn a bit more about these languages and sort of prove to myself that all these languages are, on a fundamental level, not so different from one another.

# Sample output #

	C: Hello, World!
	C: Linking...
	C++: Hello, Worlds!
	Lua: Embedded and ready
	JavaScript: Ready for prototyping
	Python: Slithering in
	Java: Just in time
	Rust: Running with safely in mind
	C#: Managed code is here
	C: We have reached the end of the call stack, time to say...
	C#: Goodbye
	Rust: Goodbye
	Java: Goodbye
	Python: Goodbye
	JavaScript: Goodbye
	Lua: Goodbye
	C++: Goodbye
	C: Goodbye

# Some takeaways #

For higher level languages, Lua is the easiest to embed, by far. Javascript, another embed focused language, on the other hand, is the most complicated to embed. There are just so many hoops you have to jump through in the engines, SpiderMonkey or V8, to get anywhere.

Of the higher level languages, only Lua and Javascript (SpiderMonkey) do not get flagged by the memory leak checker after cleanup. Python, Java, and C# (Mono) all seem to leave something behind.

As you might expect, the 3 native languages work basically the same as far as linking goes.

# Interesting issues encountered #

Python ctypes had a symbol lookup issue until I changed dlopen to have RTLD_GLOBAL.

Trying to init the Java JVM led to a SIGSEGV, which actually turned out to be something documented that is supposed to happen. It occurs while it checks the processor capabilities. I investigated the Java executable source code and determined that there needs to be signal handler setup by the application.

The Mono (C#) runtime library also had a issue where it would have a SIGSEGV on initialization. Unfortunately this time it was not expected behavior. It occurred with wrappers made with the developer's own mkbundle as well. It turned out that the garbage collector for the version I have from the Ubuntu 16.04 repository has a known bug from 2016. I tried the alternate garbage collector and it had its own bug from 2016. So the only thing left to do, besides using a newer version of mono, was to disable the garbage collector in this. Not a big deal for this project, but definitely not ideal for a language that normally depends on it.

# Running #

## Operation System Requirements ##

I developed this on Xubuntu 16.04. This could probably work on other unix-like systems. A Windows build would require changes.

## Dependencies ##
* CMake (version 3+)
* A C and C++ compiler (I used clang)
* Lua (5.1 used)
* SpiderMonkey 24 (libmozjs-24)
* Python3
* Java JDK (I used Java 8)
* Mono 2
* Rust compiler (rustc)

## Build and execute ##

If all the dependencies are satisfied, you should be able to just run "./run-release.sh" and the project should configure, build, and run. 

Using "./run.sh" will create and run a build with the address sanitizer installed.

### Note: Build directory limitation ###

The script files (for Lua, Javascript, and Python) are run by relative paths and not copied. So, right now, the project will only run if the build directory is directly under the repo directory.
