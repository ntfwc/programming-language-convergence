import os
import distutils.spawn
import subprocess

def FlagsForFile(filename, **kwargs):
    fileType = getFiletype(filename)
    if fileType == None:
        return {}

    flags = [
            '-x', fileType,
            '-Wall',
            '-Wextra',
            ]
    if fileType.startswith('c++'):
        flags.append('-std=c++11')

    addIncludePaths(flags, [
        '/usr/include/lua5.1',
        '/usr/include/python3.5m',
        '/usr/include/mozjs-24'
        ])
    addIncludePaths(flags, getJniIncludePaths())
    monoIncludeFlags = getMonoIncludeFlags()
    if monoIncludeFlags != None:
        flags += monoIncludeFlags

    return {
            'flags': flags
    }

def getFiletype(filename):
    extension = getExtension(filename)
    if extension == '.c':
        return 'c'
    if extension == '.h':
        return 'c-header'
    if extension == '.hpp':
        return 'c++-header'
    if extension == '.cpp':
        return 'c++'
    return None

def getExtension(filename):
    return os.path.splitext(filename)[1]

def findOpenJdkDir():
    jvmPath = "/usr/lib/jvm"
    for filename in os.listdir(jvmPath):
        if filename.startswith("java-"):
            return os.path.join(jvmPath, filename)

def getJniIncludePaths():
    openJdkDir = findOpenJdkDir()
    if openJdkDir == None:
        return None

    baseIncludePath = os.path.join(openJdkDir, "include")
    return [ baseIncludePath, os.path.join(baseIncludePath, "linux") ]

def getMonoIncludeFlags():
    pkgConfigPath = _findPkgConfigExe()
    if pkgConfigPath == None:
        return None
    try:
        return [subprocess.check_output([pkgConfigPath, "--cflags-only-I", "mono-2"]).rstrip()]
    except:
        return None

def addIncludePaths(flags, includePaths):
    if includePaths == None:
        return

    for includePath in includePaths:
        if includePath == None:
            continue

        flags.append("-I")
        flags.append(includePath)

def _findPkgConfigExe():
    return distutils.spawn.find_executable("pkg-config")
